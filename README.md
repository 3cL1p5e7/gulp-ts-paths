# gulp-ts-paths
Gulp stream to resolve imports aliases defined in 'tsconfig.paths' to relative paths.

## Install
npm install --save-dev gulp-ts-paths

## Usage
```js
let paths = require("gulp-ts-paths").default;

let config = { ... };

gulp.task("build", function () {
	let tsProject = ts.createProject(config.tsconfig);

	return gulp.src(config.tsGlob)
		// Assuming your ts config directory is "./src"
		.pipe(paths({ config: tsProject.config, baseDir: "./src" }))
		.pipe(tsProject())
		;
});
```

## Example
Given your tsconfig paths is: `{ "MyAlias": ["MyAliasFolder/MyAliasClass"] }`  
And a file, relative to tsconfig, is at: `./FileFolder/InnerFileFolder/File.ts`

Input:
```ts
import A from "./asdf";
import B from "express";
import C from "MyAlias"; // This is the defined alias we want to replace to relative path.

export class MyClass {
}
```

Output:
```ts
import A from "./asdf";
import B from "express";
import C from "../../MyAliasFolder/MyAliasClass";

export class MyClass {
}
```