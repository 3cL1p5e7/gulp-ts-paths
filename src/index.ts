import * as path from "path";
import ObjectStream, { EnteredArgs, Transform } from "o-stream";
import * as File from "vinyl";
import * as PluginError from "plugin-error";

const PLUGIN_NAME = "gulp-ts-paths";

type ImportAlias = { filePath: string, lineIndex: number, alias: string };

export interface Tsconfig {
	compilerOptions: CompilerOptions;
}

export interface CompilerOptions { paths: { [key: string]: string[] | undefined } };

export interface Params {
	config: Tsconfig | CompilerOptions,

	/** The base directory of the resolved paths. Usually the directory of the tsconfig file. */
	baseDir?: string;
}

interface FilledParams {
	compilerOptions: CompilerOptions,
	baseDir: string;
}

export default function plugin(parameters: Params): Transform {
	if (!parameters.config) {
		throw new PluginError(PLUGIN_NAME,
			"'config' parameter cannot be empty. Provide the tsconfig object or compilerOptions object.");
	}

	let compilerOptions = (parameters.config as Tsconfig).compilerOptions || parameters.config;

	if (!compilerOptions.paths) {
		throw new PluginError(PLUGIN_NAME, "Could not find property 'paths' in the given config.");
	}

	let params: FilledParams = Object.assign({ baseDir: "./", compilerOptions }, parameters);

	return ObjectStream.transform({
		onEntered: (args: EnteredArgs<File, File>) => {
			let file = args.object;

			throwIfStream(file);

			if (file.isNull() || !file.contents) {
				args.output.push(file);
				return;
			}

			if (!file.path) {
				throw new PluginError(PLUGIN_NAME, "Received file with no path. Files must have path to be resolved.");
			}

			let lines = file.contents.toString().split("\n");
			let imports = getImports(file.path, lines);

			if (imports.length === 0) {
				args.output.push(file);
				return;
			}

			resolveImports(lines, imports, params);

			file.contents = new Buffer(lines.join("\n"));
			args.output.push(file);
		}
	});
}

function getImports(filePath: string, lines: string[]): ImportAlias[] {
	let results: ImportAlias[] = [];

	for (let i = 0; i < lines.length; i++) {
		let line = lines[i];

		let importAlias = getImportInLine(filePath, line, i);

		if (!importAlias) continue;

		results.push(importAlias);
	}

	return results;
}

function getImportInLine(filePath: string, line: string, index: number): ImportAlias | null {
	let matches = line.match(/from "([^.]+)"/); // WHY ONLY DOUBLE QUOTES?! I AM SUFFER

	if (!matches) return null;

	if (matches.length > 2) {
		throw new PluginError(PLUGIN_NAME, "Multiple imports in the same line are not supported");
	}

	// Index 0 holds the match. Index 1 holds the group.
	let match = matches[1];

	return {
		lineIndex: index,
		alias: match,
		filePath: filePath
	};
}

function resolveImports(lines: string[], imports: ImportAlias[], params: FilledParams): void {
	for (let importAlias of imports) {
		let line = lines[importAlias.lineIndex];

		let aliasPaths = params.compilerOptions.paths[importAlias.alias];

		if (!aliasPaths) continue;

		if (aliasPaths.length > 1) {
			throw new PluginError(PLUGIN_NAME,
				`Multiple resolutions for the same alias is not supported. Alias: ${importAlias.alias}`);
		}

		let aliasPath = aliasPaths[0];

		let relative = path.relative(path.dirname(importAlias.filePath), params.baseDir);
		relative = path.join(relative, aliasPath);
		relative = relative.replace(/\\/g, "/");

		lines[importAlias.lineIndex] = line.replace(importAlias.alias, relative);
	}
}

function throwIfStream(file: File) {
	if (file.isStream()) {
		throw new PluginError(PLUGIN_NAME, 'Streams are not supported.');
	}
}